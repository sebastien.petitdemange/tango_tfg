import tango
import numpy
from tango.server import run
from tango.server import Device
from tango.server import attribute, command, device_property
from tango import DevState

import maestrio

PROG_NAME = "TFGPROG"

class TFG(Device):
    green_mode = tango.GreenMode.Gevent
    host_name = device_property(dtype=str, doc="maestrio host")
    NB_COUNTERS = 16 # 20 + 6
    
    def init_device(self):
        Device.init_device(self)

        self._maestrio = maestrio.Maestrio('tfg',{'host':self.host_name})
        self._mac_params = {
            'NBURSTS' : 1,
            'NFRAMES' : -1,
            'BURSTTRIGGER': "",
            }
        self._array_parms = {
            'BURSTINTERVAL' : [2000,99000],
            'BURSTPATTERN': [0x00,0xFF],
            'SUBINTERVAL' : [1000,99000],
            'PATTERN' : [0xFF,0x00],
            'CURRIDX' : [-1,-1],
            }
        self._data_cache = {}
        self._DI1 = 0           # NORMAL
        
    @attribute(dtype=int)
    def nb_bursts(self):
        return self._mac_params['NBURSTS']
    @nb_bursts.setter
    def nb_bursts(self,nb_bursts):
        self._mac_params['NBURSTS'] = nb_bursts

    @attribute(dtype=int)
    def nb_frames(self):
        return self._mac_params['NFRAMES']
    @nb_frames.setter
    def nb_frames(self,nb_frames):
        self._mac_params['NFRAMES'] = nframes

    @attribute(dtype=str)
    def program_state(self):
        return self._maestrio.get_program_state(PROG_NAME).name

    @command(dtype_in="DevVarDoubleArray",dtype_out="DevVarFloatArray")
    def SetTfuFrames0(self,frame_arg):
        """
        frame_arg -- frame1_nb,
                     dead1,dead1_out,stop_or_continue,
                     live1,live1_out,stop_or_continue,
                     frame2_nb
                     dead2,dead2_out,stop_or_continue,
                     live2,live2_out,stop_or_continue.
                     ...
                     deadn,deadn_out,stop_or_continue,
                     liven,liven_out,stop_or_continue.
              
              typically, liven == live

        """

        nb_frame_structure = len(frame_arg) // 7
        print(f"frame_arg: {frame_arg}")
        frame_nb  = frame_arg[0]
        dead1 = int(frame_arg[1] * 1e6)
        dead1_out = frame_arg[2]
        live_time = int(frame_arg[4] * 1e6)
        live_out = frame_arg[5]
        print(f"frame_nb:{frame_nb}\tdead1:{dead1}\tdead1_out:{dead1_out}\tlive_time:{live_time}\tlive_out:{live_out}") 

        self._array_parms['BURSTINTERVAL'] = [dead1,live_time]
        self._array_parms['BURSTPATTERN'] = [int(dead1_out) & 0xFF,int(live_out) & 0xFF]
        
        if nb_frame_structure == 1:
            self._mac_params['NFRAMES'] = int(frame_nb)
            self._mac_params.pop('NVARFRAMES',None)
            self._array_parms.pop('VARTIME',None)
            
            print('MAC PARAMS',self._mac_params)
            return [dead1 / 1e6,live_time / 1e6] * int(frame_nb)
        else:
            total_frames_number = frame_nb
            deadtimes = [dead1] * int(frame_nb)
            for frame_nb,d_time,d_out,_,l_time,l_out,_ in zip(*[iter(frame_arg[7:])] * 7):
                #check that live_time is the same
                l_time *= 1e6
                if l_time != live_time:
                    raise RuntimeError(f"Can not manage this for now, frames doesn't have the same live time {l_time} and {live_time}")
                d_time *= 1e6
                deadtimes.extend([int(d_time)] * int(frame_nb))
                total_frames_number += frame_nb

            self._mac_params['NFRAMES'] = int(total_frames_number)

            if all(int(d_time * 1e6) == x for x in deadtimes): # all deedtime are equal
                self._array_parms.pop('VARTIME',None)
                self._array_parms['SUBINTERVAL'] = [d_time, l_time]
                self._mac_params['NVARFRAMES'] = 0
            else:
                self._array_parms['SUBINTERVAL'] = [0, l_time]
                self._array_parms['VARTIME'] = deadtimes
                self._mac_params['NVARFRAMES'] = len(deadtimes)

            self._array_parms['PATTERN'] = [int(d_out) & 0xFF,int(l_out) & 0xFF]
            return_list = []
            for d,l in zip(deadtimes,[live_time] * int(total_frames_number)):
                return_list.extend((d / 1e6 ,l / 1e6))
            print(f"return_list {return_list}")
            return return_list

        # The returned timing_array contains alternating the programmed dead and live frames
        # The idea is getting the actually programmed timing from the device
        # Ideally, it should not be constructed from the input framing array, but from
        # the actually programmed timing sequence 
        #
        # nframes=1
        # dead_time timing_array[0]
        # live_time timing_array[1]
        #
        # nframes=2
        # dead_time timing_array[2]
        # live_time timing_array[3]
        #
        # ...
        # nframes=n
        # dead_time timing_array[(n-1)*2]   
        # live_time timing_array[(n-1)*2+1]


    @command(dtype_in="DevLong",dtype_out="DevLong")
    def SetTfuCycles(self,no_of_cycles):
        """
        no_of_cycles -- number of cycles should this is repeated
        """
        return 0

    @command(dtype_out="DevVarLongArray")
    def GetCompleteConfigAndStatus(self):
        """
        status_array -- ?
        """
        status = [0] * 70       # ?????:-(

        # master(0) or slave(1)
        status[0] = 0       # ????
        #time(0) or event(1) used for master
        status[1] = 0       # always the case for maestrio
        # flag saying if external start(master) or frame zero(slave)
	# enabled(1)/disabled(0)
        status[2] = 0
        # flag saying if external pause(master) or ext.inhibit(slave)
	# enabled(1)/disabled(0)
        status[3] = 0
        # polarity of external start(master) or frame zero(slave)
	# normal(0)/inversed(1)
        status[4] = 0
        # polarity of event(master) or frame clock(slave)
	# normal(0)/inversed(1)
        status[5] = 0
        # polarity of external pause(master) or ext.inhibit(slave)
	# normal(0)/inversed(1)
        status[6] = 0
        # polarity of C111 sequencer output: normal(0)/inversed(1)
	# (makes sense only for master)
        status[7] = 0
        # 50 Ohms on TFU control inputs: off(0)/on(1)
        status[8] = 0
        # Info on enabled(1)/disabled(0) interrupts
        status[9] = 0
        status[10] = 0
        status[11] = 0
        status[12] = 0
        # flag indicating if frame information is valid(1) or not(0)
        # don't know what really is (TODO)
        status[13] = 1
        # total nb of frames (makes sense only for master)
        status[14] = self._mac_params['NFRAMES'] * 2
        # total nb of live frames (makes sense only for master)
        status[15] = self._mac_params['NFRAMES']

        # total nb of "internal pauses" = pauses set in frame memory 
        # (makes sense only for master)
        # Don't know what is it (TODO), set to 0 for now
        status[16] = 0

        # total nb of cycles set (makes sense only for master)
        status[17] = self._mac_params['NBURSTS']

        # estimated time needed for 1 cycle -> makes sense when TFU master
        # and when counts time
        # TODO check what is it, set for now to 1
        status[18] = 1

        # nb of channels: TFU output port, Digital input, Analog input
        # Each of these is 8 on C216 and 4 on P216.
        # On maestrio we have 6 channels and 20 input
        status[19] = self.NB_COUNTERS

        #for compatibility reason 20:60 will be kept at 0
        #which correspond to channel config
        #status[20:60] == 0

        # currframe 60
        # currcycle 61
        status[61],status[60] = self._get_currcycle_and_frame()

        # current TFU state
        # No info in the maestrio for this
        # status[62:64] = 0

        # bank currently used for acquisition
        # Don't have this info
        # status[65] = 0

        # interrupt counts
        # Don't have this info
        # status[66:69] = 0

        return status


    
    @command(dtype_in="DevVarLongArray")
    def SetTfuSignalsPolarities(self,polarities):
        """
        should do nothing
        """
        self._DI1 = polarities[1]


    @command(dtype_in="DevLong")
    def SetTfuInputsFiftyOhms(self,input_index):
        """
        should do nothing
        """
        print("SetTfuInputsFiftyOhms input_index",input_index)
        ttl_mode = "TTL50" if input_index else "TTL"
        mode = "INV" if self._DI1 else "NORMAL"
        self._maestrio.putget("#DICFG DI1 %s %s" % (ttl_mode,mode))

        
    @command(dtype_in="DevVarLongArray")
    def SetDigitalInputsConfig(self,cfg):
        """
        should do nothing
        """
        pass

    @command(dtype_in="DevVarLongArray")
    def SetAnalogInputsConfig(self,cfg):
        """
        should do nothing
        """
        pass

    @command(dtype_in="DevLong",dtype_out="DevVarLongArray")
    def ReadScalersForOneLiveFrame(self,index):
        """
        index -- ?
        return a data array -- ?
        """
        print("ReadScalersForOneLiveFrame,{} BEGIN".format(index))

        data = self._data_cache.get(index)
        if data is None:
            local_data = []
            for local_index in range(len(self._data_cache),index+1):
                try:
                    local_data = self._maestrio.putget("?*DAQDATA 1")
                    self._data_cache[local_index] = local_data
                except RuntimeError:
                    local_data = []
                    break
            data = local_data
        # skip USERVAL and TIMER
        print("ReadScalersForOneLiveFrame,{} END".format(data))
        if len(data):
            output_data = numpy.zeros((self.NB_COUNTERS,),dtype=numpy.int32)
            #output_data[2:] = data[2:2+self.NB_COUNTERS-2]
            #output_data[1] = data[-1]
            #output_data[0] = data[20]
            #PREVIOUS MAPPING
            #output_data[1:] = data[2:2+self.NB_COUNTERS-1]
            #output_data[0] = data[21]
            #END PREVIOUS MAPPING
            output_data = data[2:2+self.NB_COUNTERS]
        else:
            output_data = []
        return output_data

    @command(dtype_in="DevVarLongArray",dtype_out="DevVarULongArray")
    def ReadScalersForNLiveFrames(self,index):
        """
        index -- start,end (end is include)
        return a data array -- ?
        """
        rdata = []
        start_index,end_index = index
        for i in range(start_index,end_index+1):
            data = self.ReadScalersForOneLiveFrame(i)
            rdata.extend(data)
        return rdata

    @command(dtype_out="DevLong")
    def GetTfuCurrentCycle(self):
        """
        return the current cycle
        """
        currcycle,currframe =  self._get_currcycle_and_frame()
        return currcycle

    @command(dtype_out="DevLong")
    def GetTfuCurrentFrame(self):
        """
        
        """
        currcycle,currframe =  self._get_currcycle_and_frame()
        if currframe < 0:
            currframe = self._mac_params['NFRAMES'] - 1
        return 2*currframe+1

    @command()
    def InitializeMemory(self):
        """
        here we do the prepare
        """
        try:
            self.prepare()
        except:
            import traceback
            traceback.print_exc()
            raise

    @command(dtype_out='DevVarLongArray')
    def GeneralCubStatus(self):
        """
        Seams to be voltage status but what should we return for maestrio?
        """
        return [1] * 8

    @command(dtype_out=str,dtype_in=str)
    def CompStatus(self,status_in):
        if status_in=="Tango::RUNNING":
            prg_status = self._maestrio.get_program_state(PROG_NAME)
            if prg_status == maestrio.PROG_STATE.RUN:
                return "Tango::RUNNING"
        
        return "Tango::ON"
    @command(dtype_out="DevVarLongArray")
    def GetTfuInterruptCounts(self):
        """
        CycleEnd
        FrameEnd
        ?
        trigger received 1 -> ok 0 -> not yet
        """
        interrupts = [0] * 4
        currcycle,currframe =  self._get_currcycle_and_frame()
        interrupts[3] = 0 if currframe < 0 else 1
        return interrupts

    @command(dtype_out="DevVarFloatArray")
    def GetTfuFrameTimes(self):

        """
        Get timing -- ?
        timing_info[] is updated with the actually programmed dead and live times, e.g.
          timing_info["0"] = 0.00100000004749745
          timing_info["1"] = 0.00100000004749745
          timing_info["2"] = 0.5
          timing_info["3"] = 0.00100000004749745
          ...
          timing_info["39"] = 0.00100000004749745

        The total number of frames, dead + live, is returned, e.g.
          40
        
        """
        nbframes = self._mac_params['NFRAMES']
        times = numpy.zeros((nbframes*2,),dtype=numpy.float32)
        vartime = self._array_parms.get('VARTIME')
        if vartime is not None:
            times[0:len(times):2] = vartime
        else:
            times[0:len(times):2] = self._array_parms['SUBINTERVAL'][0]
        times[1:len(times):2] = self._array_parms['SUBINTERVAL'][1]
        times[0] = self._array_parms['BURSTINTERVAL'][0]
        return times*1e-6
            
    
    @command()
    def prepare(self):
        print('In prepare')
        self._data_cache = {}
        try:
            self._maestrio.putget(f"PRGUNLOAD {PROG_NAME}") # TODO remove when load_program don't throw
        except:
            pass
        
        self._maestrio.set_program_mac_value(PROG_NAME,**self._mac_params)
        self._maestrio.load_program(PROG_NAME)
        self._maestrio.set_program_var_values(PROG_NAME,**self._array_parms)
        #set plucfg to default
        self._maestrio.putget("#pluprog default")

        print('Leave Prepare')

    @command(dtype_in="DevVarLongArray")
    def Start(self,params):
        external_flag = params[0]
        if external_flag:
            burst_trigger = "DI1" # extrenal start
        else:
            burst_trigger = ""  # internal
        if burst_trigger != self._mac_params['BURSTTRIGGER']:
            self._mac_params['BURSTTRIGGER'] = burst_trigger
            self.prepare()
        self._maestrio.run_program(PROG_NAME)

    @command()
    def Stop(self):
        print("Stop")
        self._maestrio.stop_program(PROG_NAME)
        self._maestrio.putget("#pluprog clear")
        #set all output to 0
        self._maestrio.putget("#OUTBIT OUTDATA 0x00")
            
    def _get_currcycle_and_frame(self):
        try:
            values = self._maestrio.get_program_var_values(PROG_NAME,"CURRIDX")
            currcycle,currframe = values["CURRIDX"]
        except RuntimeError:
            currcycle,currframe = -1,-1
        return currcycle,currframe


def main(args=None,**keys):
    keys.setdefault("green_mode",tango.GreenMode.Gevent)
    return run((TFG,),args=args,**keys)

if __name__ == "__main__":
    import sys
    main(sys.argv)
    
